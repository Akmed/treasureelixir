package me.povnu.elixir.utils;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.povnu.elixir.Main;

public class InventoryClick implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack item = e.getCurrentItem();

		if (e.getClickedInventory() == null) {
			return;
		}
			if (e.getClickedInventory().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Options.Name")))) {
				e.setCancelled(true);
			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.1.Name")))) {
				int eprice1 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.1.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice1);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir1((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound").toUpperCase()), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));
			}
			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.2.Name")))) {
				int eprice2 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.2.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice2);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir2((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}
			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.3.Name")))) {
				int eprice3 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.3.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice3);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir3((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.4.Name")))) {
				int eprice4 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.4.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice4);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir4((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.5.Name")))) {
				int eprice5 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.5.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice5);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir5((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.6.Name")))) {
				int eprice6 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.6.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice6);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir6((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.7.Name")))) {
				int eprice7 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.7.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice7);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir7((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}
			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.8.Name")))) {
				int eprice8 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.8.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice8);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir8((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
					Main.getInstance().getConfig().getString("Elixir-UI.Contents.9.Name")))) {
				int eprice9 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.9.Amount");
				Main.res = Main.econ.withdrawPlayer(p, eprice9);
				if (Main.res.transactionSuccess()) {
					p.getInventory().addItem(ElixirItem.Elixir9((Player)p));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("Messages.Purchased")));
					p.playSound(p.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Sound")), 1F, 1F);
					p.playEffect(p.getEyeLocation(), Effect.valueOf(Main.getInstance().getConfig().getString("Elixir.Options.Purchase-Effect").toUpperCase()), 1);
					return;
				}
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.No-Money")));

			}

		}
	}
}
