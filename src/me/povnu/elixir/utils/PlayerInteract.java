package me.povnu.elixir.utils;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import me.povnu.elixir.Main;

public class PlayerInteract implements Listener {

	@EventHandler
	public void onDrink(PlayerItemConsumeEvent e) {
		int eprice1 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.1.Amount") * 2;
		int chance = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.1.Chance");
		Random rand = new Random();
		int rnum = rand.nextInt(100) + 1;
		Player p = e.getPlayer();
		ItemStack consumed = e.getItem();
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.1.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance) {
				Main.res = Main.econ.depositPlayer(p, eprice1);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir1(p)));
			p.removePotionEffect(PotionEffectType.JUMP);
		}
		int eprice2 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.2.Amount") * 2;
		int chance2 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.2.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.2.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance2) {
				Main.res = Main.econ.depositPlayer(p, eprice2);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir2(p)));
			p.removePotionEffect(PotionEffectType.JUMP);
		}
		int eprice3 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.3.Amount") * 2;
		int chance3 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.3.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.3.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance3) {
				Main.res = Main.econ.depositPlayer(p, eprice3);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir3(p)));
			p.removePotionEffect(PotionEffectType.JUMP);
		}
		int eprice4 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.4.Amount") * 2;
		int chance4 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.4.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.4.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance4) {
				Main.res = Main.econ.depositPlayer(p, eprice4);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir4(p)));
			p.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
		int eprice5 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.5.Amount") * 2;
		int chance5 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.5.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.5.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance5) {
				Main.res = Main.econ.depositPlayer(p, eprice5);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir5(p)));
			p.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
		int eprice6 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.6.Amount") * 2;
		int chance6 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.6.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.6.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance6) {
				Main.res = Main.econ.depositPlayer(p, eprice6);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir6(p)));
			p.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
		int eprice7 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.7.Amount") * 2;
		int chance7 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.7.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.7.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance7) {
				Main.res = Main.econ.depositPlayer(p, eprice7);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir7(p)));
			p.removePotionEffect(PotionEffectType.HEAL);
		}
		int eprice8 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.8.Amount") * 2;
		int chance8 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.8.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.8.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance8) {
				Main.res = Main.econ.depositPlayer(p, eprice8);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir8(p)));
			p.removePotionEffect(PotionEffectType.HEAL);
		}
		int eprice9 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.9.Amount") * 2;
		int chance9 = Main.getInstance().getConfig().getInt("Elixir-UI.Contents.9.Chance");
		if (consumed.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',
				Main.getInstance().getConfig().getString("Elixir-UI.Contents.9.Name")))) {
			e.setCancelled(true);
			if (rnum <= chance9) {
				Main.res = Main.econ.depositPlayer(p, eprice9);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Win")));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("Messages.Lose")));
			}
			p.getInventory().removeItem(new ItemStack(ElixirItem.Elixir9(p)));
			p.removePotionEffect(PotionEffectType.HEAL);
		}
	}

}
