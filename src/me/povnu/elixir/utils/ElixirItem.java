package me.povnu.elixir.utils;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.povnu.elixir.Main;

public class ElixirItem {
	
	public static ItemStack Elixir1(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.1.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.1.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir2(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.2.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.2.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir3(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.3.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.3.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	public static ItemStack Elixir4(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.4.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.4.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));;
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir5(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.5.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.5.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir6(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.6.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.6.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir7(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.7.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.7.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir8(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.8.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.8.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack Elixir9(Player p) {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.9.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.9.Item-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l).replace("%player%", p.getName()));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}

}
