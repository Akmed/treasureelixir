package me.povnu.elixir.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.povnu.elixir.Main;
import me.povnu.elixir.utils.ElixirItem;

public class MainCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("Elixir")) {
			if (args.length == 0) {
				p.sendMessage(ChatColor.RED
						+ "Please finish the command with the correct arguments. /Elixir reload|give <player> <type>");
			} else {
				if (!p.hasPermission("elixir.reload")) {
					p.sendMessage(ChatColor.RED + "No Permission.");
					return true;
				}
				if (args[0].equalsIgnoreCase("reload")) {
					Main.getInstance().reloadConfig();
					p.sendMessage(ChatColor.GREEN + "You have successfully reloaded the TreasureElixir Config.");
				}
			}
			if (!p.hasPermission("elixir.give")) {
				p.sendMessage(ChatColor.RED + "No Permission.");
				return true;
			}
			if ((args.length == 3) && (args[0].equalsIgnoreCase("Give"))) {
				Player target = Bukkit.getPlayerExact(args[1]);
				if (target == null) {
					p.sendMessage(ChatColor.RED + "Sorry, That player is not online or does not exist");

				} else if (target != null) {
					if (args[2].equalsIgnoreCase("1")) {
						p.getInventory().addItem(ElixirItem.Elixir1(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("2")) {
						p.getInventory().addItem(ElixirItem.Elixir2(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("3")) {
						p.getInventory().addItem(ElixirItem.Elixir3(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("4")) {
						p.getInventory().addItem(ElixirItem.Elixir4(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("5")) {
						p.getInventory().addItem(ElixirItem.Elixir5(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("6")) {
						p.getInventory().addItem(ElixirItem.Elixir6(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("7")) {
						p.getInventory().addItem(ElixirItem.Elixir7(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("8")) {
						p.getInventory().addItem(ElixirItem.Elixir8(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));
					} else if (args[2].equalsIgnoreCase("9")) {
						p.getInventory().addItem(ElixirItem.Elixir9(target));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("Messages.Received")));

					}
				}

			}
		}
		return false;
	}
}
