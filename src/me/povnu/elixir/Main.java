package me.povnu.elixir;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.povnu.elixir.commands.MainCMD;
import me.povnu.elixir.gui.ElixirUI;
import me.povnu.elixir.utils.InventoryClick;
import me.povnu.elixir.utils.PlayerInteract;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;

public class Main extends JavaPlugin {

	private static Main instance;
	public static Economy econ = null;
	public static EconomyResponse res;

	PluginManager pm = getServer().getPluginManager();

	public void onEnable() {
		instance = this;
		// Events
		pm.registerEvents(new InventoryClick(), this);
		pm.registerEvents(new PlayerInteract(), this);
		// Commands
		getCommand("Merchant").setExecutor(new ElixirUI());
		getCommand("Elixir").setExecutor(new MainCMD());
		// Files
		getConfig().options().copyDefaults(true);
		saveConfig();

		if (!setupEconomy()) {
			Bukkit.shutdown();
			System.out.println("This plugin requires Vault! please install vault and continue.");
		}

	}

	public void onDisable() {

	}

	public static Main getInstance() {

		return instance;

	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = (Economy) rsp.getProvider();
		return econ != null;
	}

}
