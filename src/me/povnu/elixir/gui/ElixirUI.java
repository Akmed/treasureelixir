package me.povnu.elixir.gui;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.povnu.elixir.Main;

public class ElixirUI implements CommandExecutor {
	
	public static ItemStack elixir1() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.1.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.1.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir2() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.2.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.2.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir3() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8267);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.3.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.3.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	public static ItemStack elixir4() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.4.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.4.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir5() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.5.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.5.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir6() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8230);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.6.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.6.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir7() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.7.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.7.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir8() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.8.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.8.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	public static ItemStack elixir9() {
		ArrayList<String> lore = new ArrayList<>();
		ItemStack item = new ItemStack(Material.POTION, 1, (byte)8261);
		ItemMeta meta = item.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Contents.9.Name")));
		for (String l : Main.getInstance().getConfig().getStringList("Elixir-UI.Contents.9.Menu-Lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', l));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
		
	}
	
	public void elixirUI(Player p) {
		Inventory e = Bukkit.createInventory(null, InventoryType.DISPENSER, ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Elixir-UI.Options.Name")));
		
		e.setItem(0, elixir1());
		e.setItem(1, elixir2());
		e.setItem(2, elixir3());
		e.setItem(3, elixir4());
		e.setItem(4, elixir5());
		e.setItem(5, elixir6());
		e.setItem(6, elixir7());
		e.setItem(7, elixir8());
		e.setItem(8, elixir9());
		p.openInventory(e);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player)sender;
		if ((args.length == 0) && (cmd.getName().equalsIgnoreCase("Merchant"))) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("Messages.Open-Menu")));
			elixirUI((Player)sender);
			
		}
		return false;
		
	}
	
}
